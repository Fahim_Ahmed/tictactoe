﻿using System.Collections.Generic;
using UnityEngine;

namespace TTT.Game {

    /// <summary>
    /// This class is managing the scoreboard and AI moves.
    /// </summary>

    public class GameHelper {

        internal enum GAME_RESULT {
            WIN, 
            NONE, // return none when it's not a win or lose case
            DRAW
        }

        // This player pointer is only for calculation in this class
        internal enum PLAYER { A, B }

        // Grid size. E.g. 3 for 3x3 or 4 for 4x4 grid.
        int boardSize;
        
        // Keep track of scores.        
        Vector2Int[] scoreBoard;

        // Final location of tile that will return after AI complete it's calculation
        Vector2Int finalCoord;

        public GameHelper(int boardSize) {
            this.boardSize = boardSize;
            Init();
        }

        #region Internal methods

        // In each vector (x,y), x holds player A's mark and y holds player B's mark
        // Whenever a cell of vector in array reach boarsize (3 or 4) either someone wins.
        // The reason for this method is to prevent using loops for checking win/lose condition.
        // It is hard to explain in writing how this is happening though :p
        internal void UpdateScoreBoard(Vector2Int coord, PLAYER player, Vector2Int[] scores, bool removePoint = false) {
            if (scores == null) scores = scoreBoard;

            Vector2Int addPointP1 = new Vector2Int(1, 0);
            Vector2Int addPointP2 = new Vector2Int(0, 1);

            // introducing this for undoing moves
            if (removePoint) {
                addPointP1 = new Vector2Int(-1, 0);
                addPointP2 = new Vector2Int(0, -1);
            }
            
            //row
            scores[coord.x] = (player == PLAYER.A) ? scores[coord.x] + addPointP1 : scores[coord.x] + addPointP2;

            //col
            int v = boardSize + coord.y;
            scores[v] = (player == PLAYER.A) ? scores[v] + addPointP1 : scores[v] + addPointP2;

            //positive diag
            if (coord.x == coord.y) {
                v = boardSize * 2;
                scores[v] = (player == PLAYER.A) ? scores[v] + addPointP1 : scores[v] + addPointP2;
            }

            //reverse diag
            if (coord.x + coord.y == boardSize - 1) {
                v = boardSize * 2 + 1;
                scores[v] = (player == PLAYER.A) ? scores[v] + addPointP1 : scores[v] + addPointP2;
            }
        }

        // if any x or y in vectors reach boardSize(3 or 4), then that's our winner.
        // if number of moves reaches the total grid count and 
        // none of the vector reached the boardSize, then it's a draw.
        internal GAME_RESULT CheckWin(Vector2Int c, PLAYER p, int moveCount, Vector2Int[] scores) {
            if (scores == null) scores = scoreBoard;

            if (p == PLAYER.A) {
                //row
                Vector2Int v = scores[c.x];
                if (v.x == boardSize) return GAME_RESULT.WIN;

                //col
                v = scores[(boardSize) + c.y];
                if (v.x == boardSize) return GAME_RESULT.WIN;

                //diag
                v = scores[boardSize * 2];
                if (v.x == boardSize) return GAME_RESULT.WIN;

                //rev. diag
                v = scores[boardSize * 2 + 1];
                if (v.x == boardSize) return GAME_RESULT.WIN;
            }
            else {
                //row
                Vector2Int v = scores[c.x];
                if (v.y == boardSize) return GAME_RESULT.WIN;

                //col
                v = scores[(boardSize) + c.y];
                if (v.y == boardSize) return GAME_RESULT.WIN;

                //diag
                v = scores[boardSize * 2];
                if (v.y == boardSize) return GAME_RESULT.WIN;

                //rev. diag
                v = scores[boardSize * 2 + 1];
                if (v.y == boardSize) return GAME_RESULT.WIN;
            }

            if (boardSize * boardSize - 1 == moveCount) return GAME_RESULT.DRAW;

            return GAME_RESULT.NONE;
        }

        // Reset score for on game restart or replay.
        internal void ResetScores() {
            scoreBoard = new Vector2Int[boardSize * 2 + 2];
        }

        // Using Minimax algorithm with alpha-beta pruning to calculate the AI moves.
        // Right now AI is unbeatable.
        // To nerf the AI we can limit the depth of recursion.
        // Minimax algorithm starting point.
        internal Vector2Int PerformAIMove(bool isMaximizing, int moveCount, ref Dictionary<Vector2Int, Tile> tiles) {
            
            // Perform minimax on all empty tiles.
            float maxEval = -Mathf.Infinity;

            foreach (Tile t in tiles.Values) {
                if (!t.isFilled) {
                    // set a dice in a board
                    Vector2Int[] clonedScoreBoard = scoreBoard.Clone() as Vector2Int[];
                    UpdateScoreBoard(t.coord, PLAYER.B, clonedScoreBoard);

                    // mark that position as not empty
                    t.isFilled = true;

                    //evalute
                    float ev = MiniMax(t.coord, PLAYER.A, false, moveCount, 0, -Mathf.Infinity, Mathf.Infinity, ref tiles, clonedScoreBoard);

                    // compare with max score
                    if (ev >= maxEval) {
                        maxEval = ev;
                        finalCoord = t.coord;
                    }

                    // reset that position as empty
                    t.isFilled = false;
                }
            }

            return finalCoord;
        }
        #endregion

        #region Private methods

        // for draw = 0, AI wins = 1, AI lose = -1
        float MiniMax(Vector2Int coord, 
                    PLAYER player, 
                    bool isMaximizing, 
                    int moveCount, 
                    int depth,
                    float alpha,
                    float beta,
                    ref Dictionary<Vector2Int, Tile> tiles, 
                    Vector2Int[] scores) {

            // check win conditions.
            PLAYER p = (player == PLAYER.A) ? PLAYER.B : PLAYER.A;
            int res = MiniMaxResultCheck(coord, p, moveCount, scores);
            if (res == -1 || res == 0 || res == 1) {
                return res;
            }

            // Long recursion stopper
            if (depth == 6) return 1;

            // if no win or draw, start next phase.
            moveCount++;

            if (isMaximizing) {
                float maxEval = -Mathf.Infinity;

                foreach(Tile t in tiles.Values) {
                    if (!t.isFilled) {
                        // set a dice in a board
                        Vector2Int[] clonedScoreBoard = scores.Clone() as Vector2Int[];
                        UpdateScoreBoard(t.coord, player, clonedScoreBoard);

                        // mark that position as not empty
                        t.isFilled = true;

                        //evalute
                        float ev = MiniMax(t.coord, PLAYER.A, false, moveCount, depth + 1, alpha, beta, ref tiles, clonedScoreBoard);

                        // compare with max score
                        if(ev >= maxEval) {
                            maxEval = ev;
                        }
                        
                        // reset that position as empty
                        t.isFilled = false;

                        // pruning. No need to continue if already have a better value
                        alpha = Mathf.Max(ev, alpha);
                        if (beta <= alpha) break;
                    }
                }

                return maxEval;
            }
            else {
                float minEval = Mathf.Infinity;

                foreach (Tile t in tiles.Values) {
                    if (!t.isFilled) {
                        // set a dice in a board
                        Vector2Int[] clonedScoreBoard = scores.Clone() as Vector2Int[];
                        UpdateScoreBoard(t.coord, player, clonedScoreBoard);

                        // mark that position as not empty
                        t.isFilled = true;

                        //evalute
                        float ev = MiniMax(t.coord, PLAYER.B, true, moveCount, depth + 1, alpha, beta, ref tiles, clonedScoreBoard);

                        // compare with max score
                        if (ev < minEval) {
                            minEval = ev;
                        }

                        // reset that position as empty
                        t.isFilled = false;

                        // pruning. No need to continue if already have a low value
                        beta = Mathf.Min(ev, beta);
                        if (beta <= alpha) break;
                    }
                }
                
                return minEval;
            }
        }

        int MiniMaxResultCheck(Vector2Int coord, PLAYER player, int moveCount, Vector2Int[] scores) {
            var result = CheckWin(coord, player, moveCount, scores);

            if (result == GAME_RESULT.DRAW) {
                return 0;
            }

            if (result == GAME_RESULT.WIN) {
                // if user wins -1, else 1 for AI
                return (player == PLAYER.A) ? -1 : 1;
            }

            // just a random value to indicate pass
            return -10;
        }

        void Init() {
            scoreBoard = new Vector2Int[boardSize * 2 + 2]; // row count + column count + 2 diagonals
        }

        #endregion
    }
}