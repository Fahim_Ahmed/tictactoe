﻿using UnityEngine;
using TMPro;

using TTT.Player;
using TTT.Game;
using TTT.Sound;
using System;

namespace TTT.UI {

    /// <summary>
    /// Manages the screen and state of game.
    /// Show text status.
    /// Reset game UI.
    /// Communicate with player setup class to update user inputs.
    /// </summary>

    public class ScreenManager : MonoBehaviour {

        public static ScreenManager Instance { get; private set; }

        // screens or states
        public enum GAME_STATE {
            MAIN_MENU = 0,
            GAME_SETUP = 1,
            GAME_GRID_SELECTION = 2,
            IN_GAME = 3,
            GAME_OVER = 4,
            PAUSED = 5,
            RESUME = 6,
            GAME_ONLINE = 7
            //GAME_MODE_SELECTION = 7
        }

        public enum GAME_MODE {
            LOCAL = 0,
            VS_AI = 2
        }

        // screens ref.;
        public GameObject screenMainmenu;
        public GameObject screenSetup;
        public GameObject screenIngame;
        public GameObject screenGametype;
        public GameObject screenPause;
        public GameObject screenEndgame;
        public GameObject error;
        public GameObject buttonTurnEnd;

        /// It will show whose turn is currently ongoing and other error messages.
        public TextMeshProUGUI gameStatusText;
        public TextMeshProUGUI gameOverStatusText;
        
        // player move history
        public TextMeshProUGUI historyText;

        internal int boardSize = 3;
        internal GAME_MODE currentMode;

        GAME_STATE currentState;
        
        GameManager gameManager;
        Input.InputManager inputManager;
        SoundManager soundManager;
        UIPlayerSetup playerSetup;

        PlayerInfo winningPlayer;

        #region Mono methods

        void Awake() => Instance = this;

        void Start() {
            currentState = GAME_STATE.MAIN_MENU;
            
            gameManager = GameManager.Instance;
            inputManager = Input.InputManager.Instance;

            gameManager.onGameOver.AddListener(OnGameOver);
            gameManager.dispatchGameStatus.AddListener(PostInGameStatusMessage);
            gameManager.onPlayerTurnCompleted.AddListener(OnPlayerTurnCompleted);

            soundManager = SoundManager.Instance;

            playerSetup = GetComponentInChildren<UIPlayerSetup>();

            Init();
        }

        #endregion

        #region Private methods

        void Init() {
            ResetScreens();
            screenMainmenu.SetActive(true);
        }

        void ResetScreens() {
            screenMainmenu.SetActive(false);
            screenSetup.SetActive(false);
            screenIngame.SetActive(false);
            screenGametype.SetActive(false);
            screenPause.SetActive(false);
            screenEndgame.SetActive(false);
        }

        void PostInGameStatusMessage(string msg) {
            gameStatusText.text = msg;
        }

        void PostGameOverMessages(string msg) {
            gameOverStatusText.text = msg;
        }

        void OnPlayerTurnCompleted(string h) {
            UpdateHistoryText(h);
        }

        void OnGameOver(PlayerInfo winningPlayer) {
            this.winningPlayer = winningPlayer;
            ChangeStateTo(GAME_STATE.GAME_OVER);
        }

        void UpdateHistoryText(string h) {
            historyText.text += h + "\n";
        }

        void ClearHistoryText() {
            historyText.text = "";
        }

        #endregion

        #region Internal methods

        internal void ChangeStateTo(GAME_STATE state) {
            currentState = state;

            switch (currentState) {
                case GAME_STATE.MAIN_MENU:
                    ResetScreens();
                    gameManager.ResetBoard();
                    screenMainmenu.SetActive(true);
                    break;
                case GAME_STATE.GAME_SETUP:
                    ResetScreens();
                    screenSetup.SetActive(true);

                    if(currentMode == GAME_MODE.VS_AI) {
                        playerSetup.SetAIInfo();
                    }

                    break;
                case GAME_STATE.GAME_GRID_SELECTION:
                    if(playerSetup.CheckInputValidity()) {
                        ResetScreens();
                        screenGametype.SetActive(true);
                    }
                    else {
                        soundManager.PlaySound(SoundManager.SOUND_STATE.ERROR);
                    }
                    break;
                case GAME_STATE.IN_GAME:
                    ResetScreens();
                                        
                    buttonTurnEnd.SetActive(false);
                    ClearHistoryText();

                    gameManager.Init(currentMode);
                    inputManager.DeactivateInput(false);                    
                    
                    screenIngame.SetActive(true);
                    soundManager.PlaySound(SoundManager.SOUND_STATE.GAME_START);
                    break;
                case GAME_STATE.GAME_OVER:
                    ResetScreens();
                    screenEndgame.SetActive(true);

                    inputManager.DeactivateInput(true);

                    if (winningPlayer.playerName == null) {
                        Debug.Log("Draw.");
                        soundManager.PlaySound(SoundManager.SOUND_STATE.DRAW);
                        PostGameOverMessages("Game Over\nIt's a draw!");
                    }
                    else {
                        Debug.Log(winningPlayer.playerName + " wins!");
                        soundManager.PlaySound(SoundManager.SOUND_STATE.WIN);
                        PostGameOverMessages("Game Over\n" + winningPlayer.playerName + " wins!");
                    }
                    break;
                case GAME_STATE.PAUSED:
                    ResetScreens();
                    inputManager.DeactivateInput();
                    gameManager.HideGrid(true);
                    screenPause.SetActive(true);
                    break;
                case GAME_STATE.RESUME:
                    ResetScreens();
                    inputManager.DeactivateInput(false);
                    gameManager.HideGrid(false);
                    screenIngame.SetActive(true);
                    break;

            }            
        }

        internal PlayerInfo GetPlayer1() {
            return playerSetup.player1;
        }
        
        internal PlayerInfo GetPlayer2() {
            return playerSetup.player2;
        }

        #endregion

        #region From UI Binds

        // Screen changing buttons are calling this function.
        public void BtnEvent(int i) {
            ChangeStateTo((GAME_STATE) i);
            soundManager.PlaySound(SoundManager.SOUND_STATE.BUTTON_TAP);
        }

        // On end turn button pressed on UI. Not using anymore.
        public void OnEndTurn() {
            gameManager.OnEndTurn();
        }

        // On replay button pressed on UI
        public void OnReplayBtnPress() {
            gameManager.ResetBoard();
            ChangeStateTo(GAME_STATE.IN_GAME);
        }

        // On undo button pressed on UI
        public void OnUndoPressed() {
            gameManager.UndoMove();
        }
        
        // On redo button pressed on UI
        public void OnRedoPressed() {
            gameManager.RedoMove();
        }

        public void GameModeSelection(int m) {
            currentMode = (GAME_MODE) m;
            ChangeStateTo(GAME_STATE.GAME_SETUP);
            soundManager.PlaySound(SoundManager.SOUND_STATE.BUTTON_TAP);

            // For future.
            //if (currentMode == GAME_MODE.ONLINE)
            //    ChangeStateTo(GAME_STATE.GAME_ONLINE);
            //else ChangeStateTo(GAME_STATE.GAME_SETUP);
        }

        // 3x3 or 4x4 selection on UI
        public void GameGridSelection(int boardSize) {
            this.boardSize = boardSize;
            ChangeStateTo(GAME_STATE.IN_GAME);
            soundManager.PlaySound(SoundManager.SOUND_STATE.BUTTON_TAP);
        }

        public void OnCloseBtn() {
            Application.Quit();
        }

        #endregion
    }
}