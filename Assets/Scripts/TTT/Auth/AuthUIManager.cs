﻿using UnityEngine;
using TMPro;

namespace TTT.Auth {

    /// <summary>
    /// Updates the UI related to Authentication.
    /// </summary>

    public class AuthUIManager : MonoBehaviour {
        public static AuthUIManager Instance { get; private set; }

        public TextMeshProUGUI statusField;

        void Awake() => Instance = this;

        internal void SetStatusMessages(string msg) {
            statusField.text = msg;

            Invoke("ClearStatus", 3f);
        }

        void ClearStatus() {
            statusField.text = "";
        }
    }
}