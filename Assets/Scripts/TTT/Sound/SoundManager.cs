﻿using UnityEngine;

namespace TTT.Sound {

    /// <summary>
    /// Simple sound manager.
    /// </summary>

    public class SoundManager : MonoBehaviour {

        public static SoundManager Instance { get; private set; }

        internal enum SOUND_STATE {
            WIN,
            AI_WINS,
            DRAW,
            TURN_END,
            ERROR,
            PLACE_DICE,
            GAME_START,
            BUTTON_TAP
        }

        public AudioClip audioWin;
        public AudioClip audioAiWin;
        public AudioClip audioError;
        public AudioClip audioDraw;
        public AudioClip audioTurnEnd;
        public AudioClip audioPlaceDice;
        public AudioClip audioGameStart;
        public AudioClip audioBtnTap;

        AudioSource audioSource;

        void Awake() {
            Instance = this;
            audioSource = GetComponent<AudioSource>();
        }

        internal void PlaySound(SOUND_STATE state) {
            switch (state) {
                case SOUND_STATE.WIN:
                    audioSource.clip = audioWin;
                    break;
                case SOUND_STATE.AI_WINS:
                    audioSource.clip = audioAiWin;
                    break;
                case SOUND_STATE.DRAW:
                    audioSource.clip = audioDraw;
                    break;
                case SOUND_STATE.TURN_END:
                    audioSource.clip = audioTurnEnd;
                    break;
                case SOUND_STATE.ERROR:
                    audioSource.clip = audioError;
                    break;
                case SOUND_STATE.PLACE_DICE:
                    audioSource.clip = audioPlaceDice;
                    break;
                case SOUND_STATE.GAME_START:
                    audioSource.clip = audioGameStart;
                    break;
                case SOUND_STATE.BUTTON_TAP:
                    audioSource.clip = audioBtnTap;
                    break;
            }

            audioSource.Play();
        }
    }
}