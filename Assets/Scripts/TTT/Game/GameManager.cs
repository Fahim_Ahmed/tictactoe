﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

using TTT.Player;
using TTT.UI;

namespace TTT.Game {

    /// <summary>
    /// Manages the whole game-flow.
    /// Players board setup, players move.
    /// Keep move history. Undo, redo.
    /// 
    /// Note: Not using singleton pattern because it is a single scene game.
    /// </summary>

    public class GameManager : MonoBehaviour {

        public static GameManager Instance { get; private set; }

        [Tooltip("Dice prefab.")]
        public GameObject playerDicePrefab;        

        [Tooltip("Grid cell background.")]
        public GameObject tilePrefab;

        // Events
        internal UnityEventPlayerInfo onGameOver;
        internal UnityEventGameStatus dispatchGameStatus;
        internal UnityEventGameStatus onPlayerTurnCompleted;

        Board board;
        GameHelper gameHelper;

        ScreenManager screenManager;
        Sound.SoundManager soundManager;
        Input.InputManager inputManager;

        Transform gridRoot;

        // ref for current player whose turn is active
        PlayerInfo currentPlayer;

        // ref. for changing starting player's turn on game restart.
        PlayerInfo lastStartedPlayer;

        // our players
        PlayerInfo player1;
        PlayerInfo player2;
        
        bool isDicePlacedAlready = false;
        bool gameEnded = false;
        
        // keeping move for Undo, redo and draw calculation.
        int moveCount;

        // Each completed move will go here.
        Stack<PlayMove> moveHistory;

        // Each undo-ed move will go here. it'll be purged on new move.
        Stack<PlayMove> redoHistory;
        
        PlayMove currentMove;

        // whether it's vs player or AI game.
        ScreenManager.GAME_MODE currentGameMode;

        #region Mono methods

        void Awake() {
            Instance = this;            

            // Holds all the tiles and dices for easy managing.
            gridRoot = transform.Find("Grid");            

            moveHistory = new Stack<PlayMove>();
            redoHistory = new Stack<PlayMove>();

            // events
            onGameOver = new UnityEventPlayerInfo();
            dispatchGameStatus = new UnityEventGameStatus();
            onPlayerTurnCompleted = new UnityEventGameStatus();

            // First time.
            lastStartedPlayer = Random.value > 0.5f ? player1 : player2;
        }

        #endregion

        #region Private methods

        void DestroyHistory(Stack<PlayMove> history) {
            if(history.Count > 0) {
                foreach(PlayMove pm in history) {
                    Destroy(pm.diceGameObject);
                }

                history.Clear();
            }
        }

        bool CalculateTurn() {
            // add score to scorebaord
            GameHelper.PLAYER p = currentPlayer.Equals(player1) ? GameHelper.PLAYER.A : GameHelper.PLAYER.B;
            gameHelper.UpdateScoreBoard(currentMove.moveCoord, p, null);

            // calculate scores and check wins
            var result = gameHelper.CheckWin(currentMove.moveCoord, p, moveCount, null);
            if (result == GameHelper.GAME_RESULT.WIN) {
                GameOver(currentMove.player);
                return true;
            }

            // No win/lose. Check for draw.
            if(result == GameHelper.GAME_RESULT.DRAW) {
                GameOver(new PlayerInfo());
                return true;
            }
            
            // Game has not reached an end yet.
            return false;
        }

        void GameOver(PlayerInfo player) {
            onGameOver.Invoke(player);
            moveHistory.Push(currentMove);
            gameEnded = true;
        }

        void PlacePlayerMove(ref PlayMove move) {
            move.diceGameObject.transform.localPosition = board.GetTileWorldLocationFromCoord(move.moveCoord);
            soundManager.PlaySound(Sound.SoundManager.SOUND_STATE.PLACE_DICE);
        }

        // create a new move to place it on the board.
        PlayMove CreateNewMove(Vector2Int coord) {
            PlayMove pm = new PlayMove(currentPlayer, moveCount, coord);

            Vector3 worldPosition = board.GetTileWorldLocationFromCoord(currentMove.moveCoord);

            GameObject p = Instantiate(playerDicePrefab, gridRoot);
            p.transform.localPosition = worldPosition;
            p.GetComponent<SpriteRenderer>().sprite = currentPlayer.diceSprite;

            pm.diceGameObject = p;

            return pm;
        }

        void PerformAIMove() {
            Vector2Int coord = gameHelper.PerformAIMove(true, moveCount, ref board.boardTiles);
            TileSelectedAt(coord, "ai");
            OnEndTurn();
        }

        void ChangePlayer() {
            currentPlayer = currentPlayer.Equals(player1) ? player2 : player1;

            // notify UI to update text.
            dispatchGameStatus.Invoke("It's " + currentPlayer.playerName + " turn");
        }

        #endregion

        #region Internal methods

        internal void Init(ScreenManager.GAME_MODE gameMode) {
            screenManager = ScreenManager.Instance;
            soundManager = Sound.SoundManager.Instance;

            moveHistory.Clear();

            currentGameMode = gameMode;

            board = new Board(screenManager.boardSize, tilePrefab);
            gameHelper = board.gameHelper;

            board.GenerateBoard(gridRoot);

            // get player data
            player1 = screenManager.GetPlayer1();
            player2 = screenManager.GetPlayer2();

            currentPlayer = lastStartedPlayer.Equals(player1) ? player2 : player1;
            lastStartedPlayer = currentPlayer;

            isDicePlacedAlready = false;
            gameEnded = false;
            moveCount = 0;

            if (currentGameMode == ScreenManager.GAME_MODE.VS_AI && currentPlayer.Equals(player2)) {
                PerformAIMove();
            }
        }

        internal void UndoMove() {
            if (moveHistory.Count == 0) return;

            onPlayerTurnCompleted.Invoke(currentPlayer.playerName + " undoing move number " + moveCount);

            // remove the AI's move, then user's move
            if (currentGameMode == ScreenManager.GAME_MODE.VS_AI) {

                // if starting player was AI then undo will not remove AI' move.
                if (moveHistory.Count == 1 && lastStartedPlayer.Equals(player2)) return;

                // Undo AI's move
                PlayMove pm = moveHistory.Pop();            // remove from move history.
                pm.diceGameObject.SetActive(false);         // hide dice.
                board.SetTileFilled(pm.moveCoord, false);   // set the tile as empty

                // AI is always player B
                // remove score from scoreboard
                gameHelper.UpdateScoreBoard(pm.moveCoord, GameHelper.PLAYER.B, null, true);
                moveCount--;

                // Undo last User's move
                pm = moveHistory.Pop();
                pm.diceGameObject.SetActive(false);
                board.SetTileFilled(pm.moveCoord, false);

                gameHelper.UpdateScoreBoard(pm.moveCoord, GameHelper.PLAYER.A, null, true);
                moveCount--;

                // push user's move to redo history
                redoHistory.Push(pm);
            }
            else if (currentGameMode == ScreenManager.GAME_MODE.LOCAL) {
                PlayMove pm = moveHistory.Pop();
                pm.diceGameObject.SetActive(false);
                board.SetTileFilled(pm.moveCoord, false);

                GameHelper.PLAYER p = pm.player.Equals(player1) ? GameHelper.PLAYER.A : GameHelper.PLAYER.B;
                gameHelper.UpdateScoreBoard(pm.moveCoord, p, null, true);
                moveCount--;

                redoHistory.Push(pm);

                ChangePlayer();
            }


        }

        internal void RedoMove() {
            Debug.Log(redoHistory.Count);

            // check if redo is possible.
            if (redoHistory.Count > 0) {
                currentMove = redoHistory.Pop();
                isDicePlacedAlready = false;

                // trigger as if an user has clicked on a tile.
                TileSelectedAt(currentMove.moveCoord, "redo");
            }
        }

        // on user clicked on empty tile
        internal void TileSelectedAt(Vector2Int coord, string eventOrigin = "user") {
            if (gameEnded) return;

            if (isDicePlacedAlready) {
                currentMove.moveCoord = coord;
            }
            else {
                // if this is the first move in this turn, instantiate new dice gameobject
                currentMove = CreateNewMove(coord);
                isDicePlacedAlready = true;
            }

            // if an actual user performed this event. Not coming from redo move.
            if(eventOrigin == "user") DestroyHistory(redoHistory);
            PlacePlayerMove(ref currentMove);

            if (currentGameMode == ScreenManager.GAME_MODE.LOCAL) OnEndTurn();
            else if (currentGameMode == ScreenManager.GAME_MODE.VS_AI && currentPlayer.Equals(player1)) OnEndTurn();
        }

        // Hide in paused screen
        internal void HideGrid(bool hide) {
            gridRoot.gameObject.SetActive(!hide);
        }

        // reset everything for restart game.
        internal void ResetBoard() {
            Debug.Log("Destryoing everything you've built from the ashes.");

            foreach(Transform t in gridRoot) {
                Destroy(t.gameObject);
            }

            if(gameHelper != null) gameHelper.ResetScores();
            if (board != null) board.ResetBoard();
            HideGrid(false);

            DestroyHistory(redoHistory);
            DestroyHistory(moveHistory);

        }

        // trigger evaluation after an user has play it's turn.
        internal void OnEndTurn() {
            if (isDicePlacedAlready) {
                board.SetTileFilled(currentMove.moveCoord, true);
                moveHistory.Push(currentMove);
                onPlayerTurnCompleted.Invoke(currentMove.player.playerName + " (x,y): (" + currentMove.moveCoord.x + ", " + currentMove.moveCoord.y + ")");

                bool isEnded = CalculateTurn();
                if (isEnded) return;

                ChangePlayer();

                isDicePlacedAlready = false;
                moveCount++;

                // AI's turn
                if (currentGameMode == ScreenManager.GAME_MODE.VS_AI && currentPlayer.Equals(player2)) {
                    PerformAIMove();
                }
            }
            else {
                dispatchGameStatus.Invoke("You haven't play your move yet!");
                soundManager.PlaySound(Sound.SoundManager.SOUND_STATE.ERROR);
            }            
        }

        #endregion
    }

    // Keep all the information of a single move.
    public struct PlayMove {
        public PlayerInfo player;
        public int moveNumber;
        public Vector2Int moveCoord;

        public GameObject diceGameObject;

        public PlayMove(PlayerInfo p, int moveNum, Vector2Int coord) {
            player = p;
            moveNumber = moveNum;
            moveCoord = coord;
            diceGameObject = null;
        }
    }

    #region Event classes    
    public class UnityEventPlayerInfo : UnityEvent<PlayerInfo> { }
    public class UnityEventGameStatus: UnityEvent<string> { }

    #endregion
}