﻿using UnityEngine;
using TTT.Game;

namespace TTT.Input {

    /// <summary>
    /// Handles the user's input events.
    /// </summary>

    public class InputManager : MonoBehaviour {
        public static InputManager Instance { get; private set; }

        GameManager gameManager;
        bool deactiveInput = true;

        void Awake() => Instance = this;

        void Start() {
            gameManager = GameManager.Instance;
        }

        // Deactivate input events when not in game mode.
        internal void DeactivateInput(bool deactivate = true) {
            deactiveInput = deactivate;
        }

        void Update() {
            if (!deactiveInput) {
                if (UnityEngine.Input.GetKeyUp(KeyCode.U)) {
                    gameManager.UndoMove();
                }

                if (UnityEngine.Input.GetKeyUp(KeyCode.R)) {
                    gameManager.RedoMove();
                }

                if (UnityEngine.Input.GetKeyUp(KeyCode.Space)) {
                    gameManager.OnEndTurn();
                }
            }
        }
    }
}