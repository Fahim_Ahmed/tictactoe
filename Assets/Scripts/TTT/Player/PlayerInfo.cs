﻿using UnityEngine;

namespace TTT.Player {

    /// <summary>
    /// Player information holder
    /// </summary>

    public struct PlayerInfo {
        internal enum SYMBOL {
            CHIP, CLUB, DIAMOND, HEART
        }

        internal string playerName;
        internal SYMBOL symbol;
        
        internal Sprite diceSprite;
    }
}