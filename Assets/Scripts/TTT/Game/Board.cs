﻿using System.Collections.Generic;
using UnityEngine;

namespace TTT.Game {

    /// <summary>
    /// This class generate game board.
    /// Keep tiles refs and coords.
    /// Return the world location of each tiles.
    /// </summary>
    
    public class Board {

        // Grid's cell prefab.
        GameObject tilePrefab;

        // Grid size. E.g. 3 for 3x3 or 4 for 4x4 grid.
        int boardSize = 3;
        
        // gap between eahc tile
        float offset = 1.15f;
                
        internal GameHelper gameHelper;        
        // ref. list of generated tiles
        internal Dictionary<Vector2Int, Tile> boardTiles;

        public Board(int boardSize, GameObject tilePrefab) {
            this.tilePrefab = tilePrefab;
            this.boardSize = boardSize;

            gameHelper = new GameHelper(boardSize);
            boardTiles = new Dictionary<Vector2Int, Tile>();
        }

        #region Internal methods

        // Grid Coords for 3x3
        // 20 21 22
        // 10 11 12
        // 00 01 02
        internal void GenerateBoard(Transform transform) {
            boardTiles.Clear();

            // To center the board.
            float gridOffset = offset * (boardSize - 1) * 0.5f;
            transform.localPosition = new Vector3(-gridOffset, -gridOffset);

            // placing board cells.
            for (int i = 0; i < boardSize; i++) {
                for (int j = 0; j < boardSize; j++) {
                    GameObject t = GameObject.Instantiate(tilePrefab, transform);
                    t.transform.localPosition = new Vector3(i * offset, j * offset, 0);

                    var coord = new Vector2Int(i, j);
                    Tile tile = t.GetComponent<Tile>();
                    tile.coord = coord;

                    t.name = "tile_" + coord;
                    
                    boardTiles.Add(coord, tile);
                }
            }
        }

        internal Vector3 GetTileWorldLocationFromCoord(Vector2Int coord) {
            float gridOffset = offset * (boardSize - 1) * 0.5f;
            return (Vector2) coord * offset; // - (Vector2.one * gridOffset);
        }

        internal void SetTileFilled(Vector2Int coord, bool filled) {
            var tile = boardTiles[coord];
            tile.isFilled = filled;
        }

        internal void ResetBoard() {
            if(boardTiles != null) {
                boardTiles.Clear();
            }
        }

        #endregion

    }
}