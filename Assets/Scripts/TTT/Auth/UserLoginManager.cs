﻿using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;

namespace TTT.Auth {

    /// <summary>
    /// Login system using Playfab.
    /// Right now it's not prevent anyone from playing
    /// in case of login failure.
    /// </summary>

    public class UserLoginManager : MonoBehaviour {

        AuthUIManager uiManager;

        string username;
        string keyUsername = "hiddenUsername";
        string playfabId;

        void Start() {
            uiManager = AuthUIManager.Instance;
            username = PlayerPrefs.GetString(keyUsername, "");
            Login();
        }

        void Login() {
            uiManager.SetStatusMessages("Logging in . . .");

            if (username == string.Empty) { 
                username = SystemInfo.deviceUniqueIdentifier + Random.Range(0, 9000);
                PlayerPrefs.SetString(keyUsername, username);
                PlayerPrefs.Save();
            }

            LoginWithCustomIDRequest loginRequest = new LoginWithCustomIDRequest();
            loginRequest.CustomId = username;
            loginRequest.CreateAccount = true;
            
            PlayFabClientAPI.LoginWithCustomID(loginRequest, result => {
                Debug.Log("Success.");
                playfabId = result.PlayFabId;

                uiManager.SetStatusMessages("Successfully logged in!");

                //uiManager.SetStatusMessages("Getting your display name . . .");
                //if (!result.NewlyCreated) GetDisplayeName();

            }, error => {
                Debug.Log(error.Error + ": " + error.ErrorMessage);
                uiManager.SetStatusMessages(error.Error + ": " + error.ErrorMessage);
            });
        }

        internal void UpdateUserDisplayName(string name) {
            UpdateUserTitleDisplayNameRequest r = new UpdateUserTitleDisplayNameRequest();
            r.DisplayName = name;
            PlayFabClientAPI.UpdateUserTitleDisplayName(r, result => {
                Debug.Log("Name changed.");
            }, error => {
                Debug.Log(error.Error + ": " + error.ErrorMessage);
            });
        }

        internal void GetDisplayeName() {
            GetAccountInfoRequest req = new GetAccountInfoRequest();
            req.PlayFabId = playfabId;

            PlayFabClientAPI.GetAccountInfo(req, result => {
                Debug.Log(result.AccountInfo.TitleInfo.DisplayName);
                
                //uiManager.SetDisplayName(result.AccountInfo.TitleInfo.DisplayName);
                uiManager.SetStatusMessages("");

            }, error => {
                Debug.Log(error.Error + ": " + error.ErrorMessage);
            });
        }
    }
}