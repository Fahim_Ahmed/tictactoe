﻿using UnityEngine;

namespace TTT.Game {

    /// <summary>
    /// This class stores the tile information.
    /// Also mouse event listener.
    /// </summary>

    public class Tile : MonoBehaviour {
        internal Vector2Int coord;
        internal bool isFilled;

        GameManager manager;

        void Awake() {
            isFilled = false;
            manager = GameManager.Instance;
        }

        void OnMouseUp() {
            if (!isFilled) {
                manager.TileSelectedAt(coord);
            }
        }
    }
}