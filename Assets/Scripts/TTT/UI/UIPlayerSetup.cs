﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

using TTT.Player;

namespace TTT.UI {

    /// <summary>
    /// Manages all UI related functions only.
    /// Player structs creation, set dice and their sprites etc.
    /// </summary>

    public class UIPlayerSetup : MonoBehaviour {
        
        // dice sprites ref.
        public Sprite spriteChip;
        public Sprite spriteClub;
        public Sprite spriteHeart;
        public Sprite spriteDiamond;

        // dice base prefab for instantiation
        public GameObject dicePrefab;

        // players ref.
        internal PlayerInfo player1;
        internal PlayerInfo player2;

        // dice selection group
        ToggleGroup playerOneToggle;
        ToggleGroup playerTwoToggle;

        // player name fields ref.
        TMP_InputField player1NameField;
        TMP_InputField player2NameField;

        // error message field's ref.
        TextMeshProUGUI errorText;

        #region Mono methods

        void Awake() {
            player1 = new PlayerInfo();
            player2 = new PlayerInfo();

            playerOneToggle = transform.Find("Left").GetComponent<ToggleGroup>();
            playerTwoToggle = transform.Find("Right").GetComponent<ToggleGroup>();

            player1NameField = playerOneToggle.GetComponentInChildren<TMP_InputField>();
            player2NameField = playerTwoToggle.GetComponentInChildren<TMP_InputField>();

            errorText = transform.Find("Error").GetComponent<TextMeshProUGUI>();
        }

        // reset player 2 name on returning to this screen.        
        void OnEnable() {
            if (player2NameField != null) {
                player2NameField.enabled = true;
                player2NameField.text = "Player 2";
            }

            errorText.text = "";
        }

        #endregion

        #region Internal methods

        // Set AI's name mainly.
        internal void SetAIInfo() {
            player2NameField.text = "Innocent AI";
            player2NameField.enabled = false;
        }

        // checks if names are empty or both has picked same dice.
        internal bool CheckInputValidity() {
            player1 = SetSymbol(player1, playerOneToggle);
            player2 = SetSymbol(player2, playerTwoToggle);

            if(player1.symbol == player2.symbol) {                
                errorText.text = "Both cannot have same dice.";
                return false;
            }

            if(player1NameField.text.Trim().Length == 0) {
                errorText.text = "Invalid Player 1 name.";
                return false;
            }
            
            if(player2NameField.text.Trim().Length == 0) {
                errorText.text = "Invalid Player 2 name.";
                return false;
            }

            // no error, continue.
            SetPlayerData();

            return true;
        }

        // update player object data with input data
        internal void SetPlayerData() {
            player1.playerName = player1NameField.text;
            player2.playerName = player2NameField.text;
        }

        internal PlayerInfo SetSymbol(PlayerInfo pi, ToggleGroup tg) {
            IEnumerable<Toggle> toggles = tg.ActiveToggles();

            foreach (Toggle t in toggles) {
                switch (t.name) {
                    case "chip":
                        pi.symbol = PlayerInfo.SYMBOL.CHIP;
                        pi.diceSprite = spriteChip;
                        break;
                    case "club":
                        pi.symbol = PlayerInfo.SYMBOL.CLUB;
                        pi.diceSprite = spriteClub;
                        break;
                    case "heart":
                        pi.symbol = PlayerInfo.SYMBOL.HEART;
                        pi.diceSprite = spriteHeart;
                        break;
                    case "diamond":
                        pi.symbol = PlayerInfo.SYMBOL.DIAMOND;
                        pi.diceSprite = spriteDiamond;
                        break;
                }
            }

            return pi;
        }

        #endregion
    }
}